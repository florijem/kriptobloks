kriptobloks
===========

Se prononce `cryptoblox`.

Il s'agit d'une bibliothèque de fonctions en PHP pour faire de la  
cryptographie avec la sécurité la plus avancée proposée par la version  
de PHP courante.

Usage
-----

La version `7.0.0` de PHP est requise.

- La bibliothèque `OpenSSL` est requise pour une version de PHP  
inférieure à `7.2.0`.

- La bibliothèque `Libsodium` est requise pour une version PHP  
supérieure ou égale à `7.2.0`.

Inclure `kriptobloks` dans un source PHP avec l'une des possibilités  
suivantes :

```php
include '/chemin/kriptobloks.php';
include_once '/chemin/kriptobloks.php';
require '/chemin/kriptobloks.php';
require_once '/chemin/kriptobloks.php';
```

Créer une instance de `kriptobloks` :

```php
$k = new Kriptobloks();
```

Pour forcer une version de PHP :

```php
$k = new Kriptobloks('7.2.9');
```

Documentation
-------------

- encode_base64 :

    ```php
    // $k->encode_base64 ( string ) -> string
    $k->encode_base64 ( $text );
    ```

- decode_base64 :

    ```php
    // $k->decode_base64 ( string ) -> string
    $k->decode_base64 ( $text );
    ```

- encrypt :

    ```php
    // $k->encrypt ( string , string [ , string = '' ] ) -> string || false
    $k->encrypt ( $data , $key );
    $k->encrypt ( $data , $key , $additional_data );
    ```

- decrypt :

    ```php
    // $k->decrypt ( string , string [ , string = '' ] ) -> string || false
    $k->decrypt ( $data , $key );
    $k->decrypt ( $data , $key , $additional_data );
    ```

- hash :

    ```php
    // $k->hash ( string [ , string = null ] ) -> string
    $k->hash ( $data );
    $k->hash ( $data , $key );
    ```

- check_hash :

    ```php
    // $k->check_hash ( string , string [ , string = null ] ) -> boolean
    $k->check_hash ( $hashed , $data );
    $k->check_hash ( $hashed , $data , $key );
    ```

- hash_pass :

    ```php
    // $k->hash_pass ( string ) -> string
    $k->hash_pass ( $password );
    ```

- check_pass :

    ```php
    // $k->check_pass ( string , string ) -> boolean
    $k->check_pass ( $hashed_pass , $password );
    ```

Licence
-------

Copyright ou © ou Copr. Florijem, <florijem@ideovif.net>, 23-08-2018. 

Cette bibliothèque est un ensemble de fonctions en PHP pour faire de la  
cryptographie avec la sécurité la plus avancée proposée par la version  
de PHP courante.

Ce logiciel est régi par la licence CeCILL-B soumise au droit français et  
respectant les principes de diffusion des logiciels libres. Vous pouvez  
utiliser, modifier et/ou redistribuer ce programme sous les conditions  
de la licence CeCILL-B telle que diffusée par le CEA, le CNRS et l'INRIA  
sur le site <http://www.cecill.info>.

En contrepartie de l'accessibilité au code source et des droits de copie,  
de modification et de redistribution accordés par cette licence, il n'est  
offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,  
seule une responsabilité restreinte pèse sur l'auteur du programme,  le  
titulaire des droits patrimoniaux et les concédants successifs.

A cet égard  l'attention de l'utilisateur est attirée sur les risques  
associés au chargement,  à l'utilisation,  à la modification et/ou au  
développement et à la reproduction du logiciel par l'utilisateur étant  
donné sa spécificité de logiciel libre, qui peut le rendre complexe à  
manipuler et qui le réserve donc à des développeurs et des professionnels  
avertis possédant  des  connaissances  informatiques approfondies.  Les  
utilisateurs sont donc invités à charger  et  tester  l'adéquation  du  
logiciel à leurs besoins dans des conditions permettant d'assurer la  
sécurité de leurs systèmes et ou de leurs données et, plus généralement,  
à l'utiliser et l'exploiter dans les mêmes conditions de sécurité.

Le fait que vous puissiez accéder à cet en-tête signifie que vous avez  
pris connaissance de la licence CeCILL-B, et que vous en avez accepté les  
termes.
