kriptobloks
===========

Is pronounced `cryptoblox`.

This is a library of functions in PHP to make cryptography with the most  
advanced security proposed by the current PHP version.

Usage
-----

The PHP version `7.0.0` is required.

- The library `OpenSSL` is required for a PHP version less than `7.2.0`.

- The library `Libsodium` is required for a PHP version greater than or  
equal to `7.2.0`.

Include `kriptobloks` in a PHP source with one of the following  
possibilities :

```php
include '/path/kriptobloks.php';
include_once '/path/kriptobloks.php';
require '/path/kriptobloks.php';
require_once '/path/kriptobloks.php';
```

Create a new instance of `kriptobloks` :

```php
$k = new Kriptobloks();
```

To force the PHP version :

```php
$k = new Kriptobloks('7.2.9');
```

Documentation
-------------

- encode_base64 :

    ```php
    // $k->encode_base64 ( string ) -> string
    $k->encode_base64 ( $text );
    ```

- decode_base64 :

    ```php
    // $k->decode_base64 ( string ) -> string
    $k->decode_base64 ( $text );
    ```

- encrypt :

    ```php
    // $k->encrypt ( string , string [ , string = '' ] ) -> string || false
    $k->encrypt ( $data , $key );
    $k->encrypt ( $data , $key , $additional_data );
    ```

- decrypt :

    ```php
    // $k->decrypt ( string , string [ , string = '' ] ) -> string || false
    $k->decrypt ( $data , $key );
    $k->decrypt ( $data , $key , $additional_data );
    ```

- hash :

    ```php
    // $k->hash ( string [ , string = null ] ) -> string
    $k->hash ( $data );
    $k->hash ( $data , $key );
    ```

- check_hash :

    ```php
    // $k->check_hash ( string , string [ , string = null ] ) -> boolean
    $k->check_hash ( $hashed , $data );
    $k->check_hash ( $hashed , $data , $key );
    ```

- hash_pass :

    ```php
    // $k->hash_pass ( string ) -> string
    $k->hash_pass ( $password );
    ```

- check_pass :

    ```php
    // $k->check_pass ( string , string ) -> boolean
    $k->check_pass ( $hashed_pass , $password );
    ```

License
-------

Copyright or © or Copr. Florijem, <florijem@ideovif.net>, 23-08-2018. 

This library is a set of functions in PHP to make cryptography with the  
most advanced security proposed by the current PHP version.

This software is governed by the CeCILL-B license under French law and  
abiding by the rules of distribution of free software.  You can  use,  
modify and/ or redistribute the software under the terms of the CeCILL-B  
license as circulated by CEA, CNRS and INRIA at the following URL  
<http://www.cecill.info>.

As a counterpart to the access to the source code and  rights to copy,  
modify and redistribute granted by the license, users are provided only  
with a limited warranty  and the software's author,  the holder of the  
economic rights,  and the successive licensors  have only  limited  
liability.

In this respect, the user's attention is drawn to the risks associated  
with loading,  using,  modifying and/or developing or reproducing the  
software by the user in light of its specific status of free software,  
that may mean  that it is complicated to manipulate,  and  that  also  
therefore means  that it is reserved for developers  and  experienced  
professionals having in-depth computer knowledge. Users are therefore  
encouraged to load and test the software's suitability as regards their  
requirements in conditions enabling the security of their systems and/or  
data to be ensured and,  more generally, to use and operate it in the  
same conditions as regards security.

The fact that you are presently reading this means that you have had  
knowledge of the CeCILL-B license and that you accept its terms.
