<?php
// kriptobloks
// Copyright or © or Copr. Florijem, florijem@ideovif.net, 23-08-2018. 
// This library is a set of functions in PHP to make cryptography with the  
// most advanced security proposed by the current PHP version.
// This software is governed by the CeCILL-B license under French law and  
// abiding by the rules of distribution of free software.  You can  use,  
// modify and/ or redistribute the software under the terms of the CeCILL-B  
// license as circulated by CEA, CNRS and INRIA at the following URL  
// http://www.cecill.info.
// As a counterpart to the access to the source code and  rights to copy,  
// modify and redistribute granted by the license, users are provided only  
// with a limited warranty  and the software's author,  the holder of the  
// economic rights,  and the successive licensors  have only  limited  
// liability.
// In this respect, the user's attention is drawn to the risks associated  
// with loading,  using,  modifying and/or developing or reproducing the  
// software by the user in light of its specific status of free software,  
// that may mean  that it is complicated to manipulate,  and  that  also  
// therefore means  that it is reserved for developers  and  experienced  
// professionals having in-depth computer knowledge. Users are therefore  
// encouraged to load and test the software's suitability as regards their  
// requirements in conditions enabling the security of their systems and/or  
// data to be ensured and,  more generally, to use and operate it in the  
// same conditions as regards security.
// The fact that you are presently reading this means that you have had  
// knowledge of the CeCILL-B license and that you accept its terms.
  
mb_internal_encoding('UTF-8');
mb_http_output('UTF-8');
mb_http_input('UTF-8');
mb_language('uni');
mb_regex_encoding('UTF-8');
ob_start('mb_output_handler');

class Kriptobloks
{ private $is_7_2;
  private $is_7_1_2;
  private $is_7_1;
  
  function __construct ($version = null)
  { $wanted_version =
      ( isset($version) ? $version : PHP_VERSION );
    
    $this->is_7_2 =
      version_compare($wanted_version, '7.2.0', '>=');
    $this->is_7_1_2 =
      version_compare($wanted_version, '7.1.2', '>=');
    $this->is_7_1 =
      version_compare($wanted_version, '7.1.0', '>=');
  }

  function encode_base64 ( $text )
  { if ( $this->is_7_2 )
    { return sodium_bin2base64($text, SODIUM_BASE64_VARIANT_URLSAFE);
    } else
    { return strtr(base64_encode($text), '+/=', '._-');
    }
  }
  function decode_base64 ( $text )
  { if ( $this->is_7_2 )
    { return sodium_base642bin($text, SODIUM_BASE64_VARIANT_URLSAFE);
    } else
    { return base64_decode(strtr($text, '._-', '+/='));
    }
  }
  function encrypt ( $data, $key, $additional_data = '' )
  { if ( $this->is_7_2 )
    { $nonce_size =
        SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_IETF_NPUBBYTES;
      $key_size =
        SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_IETF_KEYBYTES;

      $salt =
        random_bytes(SODIUM_CRYPTO_PWHASH_SALTBYTES);
      $output_size =
        $nonce_size + $key_size;
      $nonce_key =
        sodium_crypto_pwhash
          ( $output_size
          , $key
          , $salt
          , SODIUM_CRYPTO_PWHASH_OPSLIMIT_INTERACTIVE
          , SODIUM_CRYPTO_PWHASH_MEMLIMIT_INTERACTIVE
          );
      $nonce =
        mb_substr($nonce_key, 0, $nonce_size, '8bit');
      $encrypt_key =
        mb_substr($nonce_key, $nonce_size, null, '8bit');        

      $encrypted_data =
        sodium_crypto_aead_chacha20poly1305_ietf_encrypt
          ( $data
          , $additional_data
          , $nonce
          , $encrypt_key
          );

      return $this->encode_base64($salt . $encrypted_data);
    } elseif ( $this->is_7_1 )
    { $key_size =
        32;
      $iteration_number =
        80000;
      $encrypt_algo =
        'aes-256-gcm';
      $hash_algo =
        'sha3-256';
      $seed_size =
        openssl_cipher_iv_length($encrypt_algo);
      $seed =
        random_bytes($seed_size);
      $ticket_size =
        16;

      if ( $this->is_7_1_2 )
      { $encrypt_key =
          hash_hkdf
            ( $hash_algo
            , $key
            , $key_size
            , 'aes-256-gcm-encryption'
            , $seed
            );
      } else
      { $encrypt_key =
          hash_pbkdf2
            ( $hash_algo
            , $key
            , $seed
            , $iteration_number
            , $key_size
            , true
            );
      }

      $encrypted_data =
        openssl_encrypt
          ( $data
          , $encrypt_algo
          , $encrypt_key
          , OPENSSL_RAW_DATA
          , $seed
          , $ticket
          , $additional_data
          , $ticket_size
          );
      
      return $this->encode_base64($seed . $ticket . $encrypted_data);
    } else
    { $key_size =
        32;
      $iteration_number =
        80000;
      $encrypt_algo =
        'aes-256-ctr';
      $hash_algo =
        'sha256';
      $seed_size =
        openssl_cipher_iv_length($encrypt_algo);
      $seed =
        random_bytes($seed_size);
      $hash_encrypt_key =
        hash_pbkdf2
          ( $hash_algo
          , $key
          , $seed
          , $iteration_number
          , ( $key_size * 2 )
          , true
          );
      $encrypt_key =
        mb_substr($hash_encrypt_key, 0, $key_size, '8bit');
      $hash_key =
        mb_substr($hash_encrypt_key, $key_size, null, '8bit');

      $encrypted_data =
        openssl_encrypt
          ( $data
          , $encrypt_algo
          , $encrypt_key
          , OPENSSL_RAW_DATA
          , $seed
          );

      $ticket =
        hash_hmac
          ( $hash_algo
          , ( $seed . $encrypted_data . $additional_data )
          , $hash_key
          , true
          );
      
      return $this->encode_base64($seed . $ticket . $encrypted_data);
    }
  }
  function decrypt ( $data, $key, $additional_data = '' )
  { $decoded_data =
      $this->decode_base64($data);
    
    if ( $this->is_7_2 )
    { $nonce_size =
        SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_IETF_NPUBBYTES;
      $key_size =
        SODIUM_CRYPTO_AEAD_CHACHA20POLY1305_IETF_KEYBYTES;
      $salt =
        mb_substr($decoded_data, 0, SODIUM_CRYPTO_PWHASH_SALTBYTES, '8bit');
      $encrypted_data =
        mb_substr
          ( $decoded_data
          , SODIUM_CRYPTO_PWHASH_SALTBYTES
          , null
          , '8bit'
          );
      $output_size =
        $nonce_size + $key_size;
      $nonce_key =
        sodium_crypto_pwhash
          ( $output_size
          , $key
          , $salt
          , SODIUM_CRYPTO_PWHASH_OPSLIMIT_INTERACTIVE
          , SODIUM_CRYPTO_PWHASH_MEMLIMIT_INTERACTIVE
          );
      $nonce =
        mb_substr($nonce_key, 0, $nonce_size, '8bit');
      $decrypt_key =
        mb_substr($nonce_key, $nonce_size, null, '8bit');

      $decrypted_data =
        sodium_crypto_aead_chacha20poly1305_ietf_decrypt
          ( $encrypted_data
          , $additional_data
          , $nonce
          , $decrypt_key
          );
    } elseif ( $this->is_7_1 )
    { $key_size =
        32;
      $iteration_number =
        80000;
      $encrypt_algo =
        'aes-256-gcm';
      $hash_algo =
        'sha3-256';
      $seed_size =
        openssl_cipher_iv_length($encrypt_algo);
      $seed =
        mb_substr($decoded_data, 0, $seed_size, '8bit');
      $ticket_size =
        16;
      $ticket =
        mb_substr($decoded_data, $seed_size, $ticket_size, '8bit');
      $encrypted_data =
        mb_substr($decoded_data, ( $seed_size + $ticket_size ), null, '8bit');

      if ( $this->is_7_1_2 )
      { $encrypt_key =
          hash_hkdf
            ( $hash_algo
            , $key
            , $key_size
            , 'aes-256-gcm-decryption'
            , $seed
            );
      } else
      { $encrypt_key =
          hash_pbkdf2
            ( $hash_algo
            , $key
            , $seed
            , $iteration_number
            , $key_size
            , true
            );
      }

      $decrypted_data =
        openssl_decrypt
          ( $encrypted_data
          , $encrypt_algo
          , $encrypt_key
          , OPENSSL_RAW_DATA
          , $seed
          , $ticket
          , $additional_data
          );
    } else
    { $key_size =
        32;
      $iteration_number =
        80000;
      $encrypt_algo =
        'aes-256-ctr';
      $hash_algo =
        'sha256';
      $seed_size =
        openssl_cipher_iv_length($encrypt_algo);
      $seed =
        mb_substr($decoded_data, 0, $seed_size, '8bit');
      $ticket_size =
        32;
      $received_data =
        mb_substr($decoded_data, $seed_size, $ticket_size, '8bit');
      $encrypted_data =
        mb_substr($decoded_data, ( $seed_size + $ticket_size ), null, '8bit');
      $hash_encrypt_key =
        hash_pbkdf2
          ( $hash_algo
          , $key
          , $seed
          , $iteration_number
          , ( $key_size * 2 )
          , true
          );
      $encrypt_key =
        mb_substr($hash_encrypt_key, 0, $key_size, '8bit');
      $hash_key =
        mb_substr($hash_encrypt_key, $key_size, null, '8bit');

      $ticket =
        hash_hmac
          ( $hash_algo
          , ( $seed . $encrypted_data . $additional_data )
          , $hash_key
          , true
          );

      if ( !hash_equals($received_data, $ticket) ) { return false; }

      $decrypted_data =
        openssl_decrypt
          ( $encrypted_data
          , $encrypt_algo
          , $encrypt_key
          , OPENSSL_RAW_DATA
          , $seed
          );
    }

    return $decrypted_data;
  }
  function hash ( $data, $key = null )
  { if ( $this->is_7_2 )
    { if ( isset($key) )
      { $ticket =
          sodium_crypto_generichash
            ( $data
            , $key
            , SODIUM_CRYPTO_GENERICHASH_BYTES
            );
      } else
      { $ticket =
          sodium_crypto_generichash($data);
      }
    } else
    { $hash_algo =
        ( $this->is_7_1 ? 'sha3-256' : 'sha256' );

      if ( isset($key) )
      { $ticket =
          hash_hmac($hash_algo, $data, $key, true);
      } else
      { $ticket =
          hash($hash_algo, $data, true);
      }
    }

    return $this->encode_base64($ticket);
  }
  function check_hash ( $hashed, $data, $key = null )
  { $decoded_hash =
      $this->decode_base64($hashed);
    
    if ( $this->is_7_2 )
    { if ( isset($key) )
      { $ticket =
          sodium_crypto_generichash
            ( $data
            , $key
            , SODIUM_CRYPTO_GENERICHASH_BYTES
            );
      } else
      { $ticket =
          sodium_crypto_generichash($data);
      }

      return (( sodium_memcmp($decoded_hash, $ticket) === 0 ) ? true : false );
    } else
    { $hash_algo =
        ( $this->is_7_1 ? 'sha3-256' : 'sha256' );

      if ( isset($key) )
      { $ticket =
          hash_hmac($hash_algo, $data, $key, true);
      } else
      { $ticket =
          hash($hash_algo, $data, true);
      }

      return hash_equals($decoded_hash, $ticket);
    }
  }
  function hash_pass ( $password )
  { if ( $this->is_7_2 )
    { return 
        sodium_crypto_pwhash_str
          ( $password
          , SODIUM_CRYPTO_PWHASH_OPSLIMIT_INTERACTIVE
          , SODIUM_CRYPTO_PWHASH_MEMLIMIT_INTERACTIVE
          );
    } else
    { $options =
        [ 'cost' => 11
        ];
      return password_hash($password, PASSWORD_BCRYPT, $options);
    }
  }
  function check_pass ( $hashed_pass, $password )
  { if ( $this->is_7_2 )
    { return sodium_crypto_pwhash_str_verify($hashed_pass, $password);
    } else
    { return password_verify($password, $hashed_pass);
    }
  }
}
?>
